// console.log("Hello World");

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

//Register

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/

let userName = [];

function registerUser(userName) {
    // userName = String(prompt("Input your username."));
    let userNameIndex = registeredUsers.indexOf(userName);
    if(userNameIndex === -1) {
        registeredUsers.push(userName);
        alert("Thank you for registering!")
    }
    else { 
        alert("Registration failed. Username already exists!")
    }
}

// registerUser();

// console.log(registeredUsers);

// Add friend to friendlist
/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

let friendName = [];

function addFriend(friendName) {
    // friendName = String(prompt("Input registered username to add as friend."));
    let friendNameIndex = registeredUsers.indexOf(friendName);
    if(friendNameIndex === -1) {
        alert("User not found.")
    }
    else {
        let friendNameIndex2 = friendsList.indexOf(friendName);
        if(friendNameIndex2 >= 0 && friendNameIndex >= 0) {
            alert(friendName+" is already a friend.")
        }
        else {
        friendsList.push(friendName);
        alert("You have added "+friendName+" as a friend!")
        }
    }
    
}

// addFriend();

// console.log(friendsList);

// Display friends - list names

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function displayFriends() {
    console.log(friendsList);
    // "You currently have 0 friends. Add one first."
}
    

// Display number of friends
/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumFriends() {
    if(friendsList.length === undefined || friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.")
    }
    else {alert("You currently have "+friendsList.length+" friends.");
    } 
}

// Delete last element
/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/

function removeFriend() {
    if(friendsList.length === undefined || friendsList.length === 0) {
        alert("You currently have 0 friends. Add one first.")
    }
    else {friendsList.pop();
    }
}


/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/

function removeSpecificFriend(friendName) {
    if(friendsList.length === 0 || friendsList.length === undefined) {
        alert("You currently have 0 friends. Add one first.")
    }
    else {let friendNameIndex = friendsList.indexOf(friendName);
        if(friendNameIndex >= 0) {
            friendsList.splice(friendNameIndex,1);
            }
        else {alert("User is not in your friends list.")
        }
    }
}




